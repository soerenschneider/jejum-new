package com.soerenschneider.jejum.jejum;

import com.soerenschneider.jejum.jejum.utils.DateRange;
import com.soerenschneider.jejum.jejum.utils.JejumDateUtils;


import org.junit.Assert;
import org.junit.Test;

import java.time.ZonedDateTime;

public class DateRangeTest {
    @Test
    public void openrange() {
        DateRange r = new DateRange();
        ZonedDateTime now = JejumDateUtils.now();
        Assert.assertTrue(now.isAfter(r.from));
        Assert.assertTrue(now.isBefore(r.to));
    }

    @Test
    public void year() {
        ZonedDateTime now = JejumDateUtils.now();
        DateRange r = new DateRange(now.getYear());
        Assert.assertTrue(now.isAfter(r.from));
        Assert.assertTrue(now.isBefore(r.to));
    }

    @Test
    public void year_previous() {
        ZonedDateTime now = JejumDateUtils.now();
        DateRange r = new DateRange(now.minusYears(1).getYear());
        Assert.assertTrue(now.isAfter(r.from));
        Assert.assertTrue(now.isAfter(r.to));
    }

    @Test
    public void year_next() {
        ZonedDateTime now = JejumDateUtils.now();
        DateRange r = new DateRange(now.plusYears(1).getYear());
        Assert.assertTrue(now.isBefore(r.from));
        Assert.assertTrue(now.isBefore(r.to));
    }

    @Test
    public void month() {
        ZonedDateTime now = JejumDateUtils.now();
        DateRange r = new DateRange(now.getYear(), now.getMonthValue());
        Assert.assertTrue(now.isAfter(r.from));
        Assert.assertTrue(now.isBefore(r.to));
    }

    @Test
    public void month_previous() {
        ZonedDateTime now = JejumDateUtils.now();
        ZonedDateTime offset = now.minusMonths(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue());

        Assert.assertTrue(now.isAfter(r.from));
        Assert.assertTrue(now.isAfter(r.to));
    }

    @Test
    public void month_next() {
        ZonedDateTime now = JejumDateUtils.now();
        ZonedDateTime offset = now.plusMonths(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue());

        Assert.assertTrue(now.isBefore(r.from));
        Assert.assertTrue(now.isBefore(r.to));
    }

    @Test
    public void week() {
        ZonedDateTime now = JejumDateUtils.now();
        DateRange r = new DateRange(now.getYear(), now.getMonthValue(), now.getDayOfMonth());

        Assert.assertTrue(now.isAfter(r.from));
        Assert.assertTrue(now.isBefore(r.to));
    }

    @Test
    public void week_prev() {
        ZonedDateTime now = JejumDateUtils.now();
        ZonedDateTime offset = now.minusWeeks(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue(), offset.getDayOfMonth());

        Assert.assertTrue(now.isAfter(r.from));
        Assert.assertTrue(now.isAfter(r.to));
    }

    @Test
    public void week_next() {
        ZonedDateTime now = JejumDateUtils.now();
        ZonedDateTime offset = now.plusWeeks(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue(), offset.getDayOfMonth());

        Assert.assertTrue(now.isBefore(r.from));
        Assert.assertTrue(now.isBefore(r.to));
    }

    @Test
    public void JejumDateUtils() {
        String sunday_rfc3339 = "2019-01-20T10:03:59.655+01:00";
        ZonedDateTime sunday = ZonedDateTime.parse(sunday_rfc3339);
        ZonedDateTime last_sunday = sunday.minusWeeks(1);

        String monday_rfc3339 = "2019-01-14T10:03:59.655+01:00";
        ZonedDateTime monday = ZonedDateTime.parse(monday_rfc3339);
        ZonedDateTime next_monday = monday.plusWeeks(1);

        DateRange r = new DateRange(sunday.getYear(), sunday.getMonthValue(), sunday.getDayOfMonth());
        Assert.assertTrue(sunday.isAfter(r.from));
        Assert.assertTrue(sunday.isBefore(r.to));

        Assert.assertTrue(monday.isAfter(r.from));
        Assert.assertTrue(monday.isBefore(r.to));

        Assert.assertTrue(last_sunday.isBefore(r.from));
        Assert.assertTrue(last_sunday.isBefore(r.to));

        Assert.assertTrue(next_monday.isAfter(r.from));
        Assert.assertTrue(next_monday.isAfter(r.to));
    }


}
