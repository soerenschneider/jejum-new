package com.soerenschneider.jejum.jejum.fasting.states;

public enum State {
    INACTIVE,
    FASTING,
    UNCONFIRMED;
}
