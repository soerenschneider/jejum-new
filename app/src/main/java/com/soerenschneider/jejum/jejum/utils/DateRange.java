package com.soerenschneider.jejum.jejum.utils;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

/**
 * Returns a left-closed, right-open interval of a date that spans over a user defined time.
 */
public class DateRange {
    private static final Instant minInstant = Instant.ofEpochMilli(Long.MIN_VALUE);
    private static final Instant maxInstant = Instant.ofEpochMilli(Long.MAX_VALUE);

    public final ZonedDateTime from;
    public final ZonedDateTime to;

    /**
     * Returns an interval that virtually includes all dates.
     */
    public DateRange() {
        this.from = minInstant.atZone(ZoneId.systemDefault());
        this.to = maxInstant.atZone(ZoneId.systemDefault());
    }

    /**
     * Returns an interval that represents the earliest and latest moment in a year.
     * @param year
     */
    public DateRange(int year) {
        this.from = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault());
        this.to = new DateRange(year, 12).to;
    }

    /**
     * Returns an interval that represents the earliest and latest moment in a year's month.
     * @param year
     */
    public DateRange(int year, int month) {
        ZonedDateTime initial = ZonedDateTime.of(year, month, 1, 0, 0, 0, 0, ZoneId.systemDefault());
        this.from = initial;
        this.to = initial.plusMonths(1);
    }

    /**
     * Returns an interval that represents the earliest and latest moment in a year's month.
     * @param year
     */
    public DateRange(int year, int month, int day) {
        ZonedDateTime initial = ZonedDateTime.of(year, month, day, 0, 0, 0, 0, ZoneId.systemDefault());
        this.from = initial.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        this.to = initial.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)).plusDays(1);
    }

    public long getUnixFrom() {
        return from.toInstant().toEpochMilli();
    }

    public String getSql() {
        return "start <= " + JejumDateUtils.toUtc(from) + " and target >= " + JejumDateUtils.toUtc(to);
    }

    public long getUnixTo() {
        return to.toInstant().toEpochMilli();
    }

    @Override
    public String toString() {
        return "DateRange{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
