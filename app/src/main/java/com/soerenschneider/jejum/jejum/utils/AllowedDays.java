package com.soerenschneider.jejum.jejum.utils;

import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class AllowedDays {
    private Map<String, ZonedDateTime> allowedDays = new LinkedHashMap<>();

    public void addDay(String formatted, ZonedDateTime day) {
        this.allowedDays.put(formatted, day);
    }

    public Map<String, ZonedDateTime> getAllowedDays() {
        return allowedDays;
    }

    public Optional<ZonedDateTime> getByKey(String formatted) {
        return Optional.of(allowedDays.get(formatted));
    }

    public String[] toDisplayableValues() {
        return allowedDays.keySet().toArray(new String[allowedDays.size()]);
    }

    public int getIndexOfPreferencedDay(Preference pref) {
        if (null != pref) {
            Iterator<ZonedDateTime> it = allowedDays.values().iterator();
            switch (pref) {
                case TODAY:
                    for (int i = 0; i < allowedDays.size(); i++) {
                        if (JejumDateUtils.isToday(it.next())) {
                            return i;
                        }
                    }
                    break;
                case YESTERDAY:
                    for (int i = 0; i < allowedDays.size(); i++) {
                        if (JejumDateUtils.isYesterday(it.next())) {
                            return i;
                        }
                    }
                    break;
                case TOMORROW:
                    for (int i = 0; i < allowedDays.size(); i++) {
                        if (JejumDateUtils.isTomorrow(it.next())) {
                            return i;
                        }
                    }
                    break;
            }
        }

        return allowedDays.size()-1;
    }

    public static enum Preference {
        TODAY,
        YESTERDAY,
        TOMORROW;
    }
}

