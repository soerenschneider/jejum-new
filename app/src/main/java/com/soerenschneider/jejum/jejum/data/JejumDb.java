package com.soerenschneider.jejum.jejum.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Record.class}, version = 1)
public abstract class JejumDb extends RoomDatabase {
    public abstract JejumDao recordsDao();
}