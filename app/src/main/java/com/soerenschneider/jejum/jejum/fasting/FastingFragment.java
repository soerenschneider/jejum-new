package com.soerenschneider.jejum.jejum.fasting;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.soerenschneider.jejum.jejum.JejumCountdownTimer;
import com.soerenschneider.jejum.jejum.data.Record;
import com.soerenschneider.jejum.jejum.utils.JejumDateUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A placeholder fragment containing a simple view.
 */
public class FastingFragment extends Fragment {

    private Context ctx;
    private JejumCountdownTimer timer;
    private FastingViewModel model;
    private String tag = "jejum";

    private final CompositeDisposable mDisposable = new CompositeDisposable();

    private ProgressBar progressIndicator;
    private TextView countdown;
    private TextView startDate;
    private TextView targetDate;
    private FloatingActionButton fab;

    public FastingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.ctx = getContext();
        System.out.println(savedInstanceState);

        model = ViewModelProviders.of(getActivity()).get(FastingViewModel.class);

        mDisposable.add(model.getRecord()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(record -> recordUpdatedCallback(record),
                        throwable -> Log.e(tag, "Exception", throwable))
        );

        View child =  inflater.inflate(R.layout.fragment_fasting, container, false);
        initUi(child);

        return child;
    }

    private void recordUpdatedCallback(Record r) {
        Log.d(tag, "Setting record to " + r);

        String from = JejumDateUtils.formatDayTime(r.getStartDate());
        startDate.setText(from);

        String targetStr = JejumDateUtils.formatDayTime(r.getTargetDate());
        targetDate.setText(targetStr);
    }

    @Override
    public void onPause() {
        super.onPause();
        cancelTimer();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("a", 1L);
    }

    private void errorNotification(String message) {
        Log.e(tag, message);
        // TODO: UI Notification
    }

    private void initUi(View view) {
        this.progressIndicator = view.findViewById(R.id.fragment_fasting_progressBar);
        this.countdown = view.findViewById(R.id.fragment_fasting_countdown);
        this.startDate = view.findViewById(R.id.fragment_fasting_start);
        this.targetDate = view.findViewById(R.id.fragment_fasting_target);
        this.fab = view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Record r  = new Record(16 * 3600, JejumDateUtils.generateStartDate());
                model.trigger();
                fab.setImageResource(R.drawable.ic_info_black_24dp);
            }
        });
    }

    private void scheduleTimer(Record r) {
        cancelTimer();

        Log.i(tag, "Scheduling timer");
        progressIndicator.setMax(r.getFastingWindowLength());

        // Perform steps + 1 to nicely finish in the ui
        this.timer = new JejumCountdownTimer(r.getTargetDate(), r.getFastingWindowLength()) {
            public void onTick(long millisUntilFinished) {
                int secondsUntilTarget = (int) JejumDateUtils.secondsUntilTarget(targetDate);
                int progress = windowLength - secondsUntilTarget;

                progressIndicator.setProgress(progress);
                countdown.setText(JejumDateUtils.getCountdown(r));
            }
        };

        this.timer.start();
    }

    private void cancelTimer() {
        Log.i("Timer", "Timer has been canceled");
        if (null != this.timer && ! this.timer.isFinished()) {
            this.timer.cancel();
            this.timer = null;
        }
    }
}
