package com.soerenschneider.jejum.jejum.fasting.states;

public enum Event {
    STARTED,
    CANCELLED,
    REACHED,
    RESET,
    CONFIRMED;
}
