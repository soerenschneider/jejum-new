package com.soerenschneider.jejum.jejum.data;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.provider.ContactsContract;

import java.net.ContentHandler;

public class DatabaseManager {
    private final Context ctx;
    private static DatabaseManager instance;
    private final JejumDb db;
    public final JejumDao recordsDao;

    public static DatabaseManager build(Context ctx) {
        if (null == instance) {
            synchronized (DatabaseManager.class) {
                if (null == instance) {
                    instance = new DatabaseManager(ctx);
                }
            }
        }

        return instance;
    }

    public DatabaseManager(Context context) {
        this.ctx = context;
        db = Room.databaseBuilder(ctx, JejumDb.class, "jejum").build();
        recordsDao = db.recordsDao();
    }
}
