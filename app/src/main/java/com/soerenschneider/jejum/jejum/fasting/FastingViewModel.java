package com.soerenschneider.jejum.jejum.fasting;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.LiveDataReactiveStreams;
import android.support.annotation.NonNull;
import android.util.Log;
import com.soerenschneider.jejum.jejum.data.DatabaseManager;
import com.soerenschneider.jejum.jejum.data.JejumDao;
import com.soerenschneider.jejum.jejum.data.Record;

import java.util.List;
import java.util.Optional;

import com.soerenschneider.jejum.jejum.utils.JejumDateUtils;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FastingViewModel extends AndroidViewModel {
    private JejumDao dao;
    private Record record;

    public FastingViewModel(@NonNull Application application) {
        super(application);
        dao = DatabaseManager.build(application.getApplicationContext()).recordsDao;
    }

    public Flowable<List<Record>> getRecords() { return dao.getRecords(); };

    public Flowable<Record> getRecord() {
        return dao.getMostRecent().map(record -> {
            this.record = record;
            return record;
        });
    }

    public void interact() {

    }

    public void resetStartTime() {
        JejumDateUtils.reschedule(record);
        update(record);
    }

    public void trigger() {

    }

    private void insert(Record r) {
        Completable.fromAction(() -> dao.insert(r))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        () -> {},
                        ex -> {
                            Log.e("jejum", ex.toString());
                        }
                );
    }

    private void update(Record r) {
        Completable.fromAction(() -> dao.updateRecords(r))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        () -> {},
                        ex -> {
                            Log.e("jejum", ex.toString());
                        }
                );
    }

}
