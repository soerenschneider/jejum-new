package com.soerenschneider.jejum.jejum.fasting.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import com.soerenschneider.jejum.jejum.R;

import java.time.ZonedDateTime;

public class NotificationScheduler {
    public void scheduleNotifications(Context ctx, ZonedDateTime target) {
        if (PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean(ctx.getResources().getString(R.string.prefs_notifications_imminent_notification), false)) {
            scheduleNotification(ctx, target, NotificationType.IMMINENT);
        }

        scheduleNotification(ctx, target, NotificationType.ACHIEVED);
    }

    public static void scheduleNotification(Context ctx, ZonedDateTime notificationDate, NotificationType type) {
        cancelScheduledNotification(ctx, type);

        Intent alarmIntent = new Intent(ctx, NotifierService.class);
        alarmIntent.putExtra("type", type.name());

        // Schedule imminent notification
        if (NotificationType.IMMINENT == type) {
            String key = ctx.getResources().getString(R.string.prefs_notifications_imminent_offset);
            int minutes = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(ctx).getString(key, "60"));
            notificationDate = notificationDate.minusMinutes(minutes);

            alarmIntent.putExtra(ctx.getResources().getString(R.string.prefs_notifications_imminent_offset), minutes);
        }

        PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx, type.code, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);

        //manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 3000 + Bla.now().toInstant().toEpochMilli(), pendingIntent);
        manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, notificationDate.toInstant().toEpochMilli(), pendingIntent);
    }

    public static void cancelScheduledNotification(Context ctx) {
        for (NotificationType type : NotificationType.values()) {
            cancelScheduledNotification(ctx, type);
        }
    }

    public static void cancelScheduledNotification(Context ctx, NotificationType type) {
        Intent alarmIntent = new Intent(ctx, NotifierService.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx, type.code, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
    }
}
