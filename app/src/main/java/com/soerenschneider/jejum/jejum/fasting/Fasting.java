package com.soerenschneider.jejum.jejum.fasting;

import android.content.Context;

import com.soerenschneider.jejum.jejum.data.Record;
import com.soerenschneider.jejum.jejum.utils.JejumDateUtils;

import java.util.Optional;

public class Fasting {
    private Context ctx;

    public Fasting(Context ctx) {
        this.ctx = ctx;
    }

    public void cancelFasting() {
    }

    public void startFasting() {
    }
}
