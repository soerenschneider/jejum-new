package com.soerenschneider.jejum.jejum.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TimePicker;

import com.soerenschneider.jejum.jejum.R;
import com.soerenschneider.jejum.jejum.utils.AllowedDays;

import java.io.OptionalDataException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

public class DateTimePickerDialog extends AlertDialog implements DialogInterface.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private AllowedDays allowedDays;
    private NumberPicker dayPicker;
    private TimePicker timePicker;
    private Switch aSwitch;

    private ZonedDateTime pickedDate;
    private ZonedDateTime defaultDate;
    private boolean useCustomDate = false;
    private DateTimeSetListener listener;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (null == dayPicker || null == timePicker) {
            return;
        }

        if (isChecked) {
            dayPicker.setVisibility(View.VISIBLE);
            timePicker.setVisibility(View.VISIBLE);
        } else {
            dayPicker.setVisibility(View.GONE);
            timePicker.setVisibility(View.GONE);
        }

        useCustomDate = isChecked;
    }

    public ZonedDateTime getDate() {
        if (useCustomDate) {
            return pickedDate;
        }
        return defaultDate;
    }

    private void readFromPickers() {
        String key = dayPicker.getDisplayedValues()[dayPicker.getValue()];
        ZonedDateTime pit = allowedDays.getByKey(key).get();
        this.pickedDate = pit.withHour(timePicker.getHour()).withMinute(timePicker.getMinute()).withSecond(0).withNano(0);
        listener.dateTimeSet(pickedDate);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == BUTTON_POSITIVE) {
            readFromPickers();
        }
    }

    @Override
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> data, @Nullable Menu menu, int deviceId) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public DateTimePickerDialog(Context context, AllowedDays allowedDays, AllowedDays.Preference displayPreference, ZonedDateTime time) {
        super(context);

        final Context themeContext = getContext();
        this.defaultDate = time;
        final LayoutInflater inflater = LayoutInflater.from(themeContext);
        final View view = inflater.inflate(R.layout.dialog_define_target, null);
        this.allowedDays = allowedDays;

        this.aSwitch = view.findViewById(R.id.dialog_define_target_switch);
        aSwitch.setOnCheckedChangeListener(this);

        this.dayPicker = view.findViewById(R.id.dialog_define_target_daypicker);
        dayPicker.setMinValue(0);
        dayPicker.setMaxValue(allowedDays.getAllowedDays().size() - 1);
        dayPicker.setValue(allowedDays.getIndexOfPreferencedDay(displayPreference));
        dayPicker.setDisplayedValues(allowedDays.toDisplayableValues());

        this.timePicker = view.findViewById(R.id.dialog_define_target_timepicker);
        timePicker.setIs24HourView(true);
        timePicker.setHour(time.getHour());
        timePicker.setMinute(time.getMinute());

        setView(view);
        setButton(BUTTON_POSITIVE, "OK", this);
        setButton(BUTTON_NEGATIVE, "Cancel", this);
    }

    public interface DateTimeSetListener {
        void dateTimeSet(ZonedDateTime dateTime);
    }
}
