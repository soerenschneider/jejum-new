package com.soerenschneider.jejum.jejum.fasting.notification;

import java.util.UUID;

public enum NotificationType {
    IMMINENT(UUID.randomUUID().hashCode()),
    ACHIEVED(UUID.randomUUID().hashCode());

    NotificationType(int code) {
        this.code = code;
    }

    int code;
}
