package com.soerenschneider.jejum.jejum.utils;

import com.soerenschneider.jejum.jejum.data.Record;
import com.soerenschneider.jejum.jejum.fasting.IFastingState;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class JejumDateUtils {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    private static final DateTimeFormatter daysWithinCurrentMonth = DateTimeFormatter.ofPattern("dd.MM.");
    private static final DateTimeFormatter daysOutsideCurrentMonth = DateTimeFormatter.ofPattern("dd.MM.YY");

    public static ZonedDateTime now() {
        return ZonedDateTime.now();
    }

    public static ZonedDateTime generateStartDate() {
        return ZonedDateTime.now().withSecond(0).withNano(0);
    }

    public static ZonedDateTime getTargetDate(long windowLengthSeconds) {
        return getTargetDate(windowLengthSeconds, ZonedDateTime.now());
    }

    public static ZonedDateTime getTargetDate(long windowLengthSeconds, ZonedDateTime from) {
        return from.plusSeconds(windowLengthSeconds).withSecond(0).withNano(0);
    }

    /**
     * Returns the duration between [from, start].
     * @param from
     * @param target
     * @return
     */
    public static Duration diff(ZonedDateTime from, ZonedDateTime target) {
        return Duration.between(from, target);
    }

    public static Optional<ZonedDateTime> fromUtc(Long utc) {
        return fromUtc(utc, ZoneId.of("UTC"));
    }

    public static Optional<ZonedDateTime> fromUtc(Long utc, ZoneId zone) {
        if (null == utc) {
            return Optional.empty();
        }
        return Optional.of(ZonedDateTime.ofInstant(Instant.ofEpochMilli(utc), ZoneId.of("UTC")));
    }


    public IFastingState.State calcState(Record r) {
        boolean actualSet = r.getActualDate().isPresent();

        if (actualSet) {
            return IFastingState.State.RESTING;
        }

        ZonedDateTime now = JejumDateUtils.now();
        boolean withinWindow = (now.isAfter(r.getStartDate()) || now.isEqual(r.getStartDate()))
                && (now.isBefore(r.getTargetDate()) || now.isEqual(r.getTargetDate()));

        // NOT actualSet AND within window
        if (withinWindow) {
            return IFastingState.State.FASTING;
        }

        // NOT actualSet AND NOT within window
        return IFastingState.State.UNCONFIRMED;
    }

    /*
    public static JejumState computeState(Record record) {
        ZonedDateTime now = now();

        ZonedDateTime start = record.getStartDate();
        ZonedDateTime target = record.getTargetDate();

        if ((now.isAfter(start) || now.isEqual(start)) && (now.isBefore(target))) {
            return JejumState.FASTING;
        }

        return JejumState.RESTING;
    }
    */

    public static String getCountdown(Record record) {
        long secondsUntilTarget = secondsUntilTarget(record.getTargetDate());

        if (secondsUntilTarget > 60) {
            int hours = (int) secondsUntilTarget / 3600;
            int mins = (int) (secondsUntilTarget % 3600) / 60;

            return String.format("%02d:%02d", hours, mins);
        } if (secondsUntilTarget > 0) {
            return "Less than a minute";
        }

        return "Inactive";
    }

    public static long toUtc(ZonedDateTime date) {
        return date.withZoneSameLocal(ZoneId.of("UTC")).toInstant().toEpochMilli();
    }

    public static boolean achievedGoal(ZonedDateTime target) {
        return diff(now(), target).isNegative();
    }

    public static boolean achievedGoal(ZonedDateTime start, ZonedDateTime target) {
        return ! diff(start, target).isNegative();
    }

    public static long secondsUntilTarget(ZonedDateTime target) {
        return Duration.between(now(), target).getSeconds();
    }

    public static float getProgress(long windowLength, ZonedDateTime targetDate) {
        return secondsUntilTarget(targetDate) * 100f / windowLength;
    }

    /**
     * Returns a map containing all dates that values that span over a duration defined by the start date and
     * its windowLengthSeconds.
     *
     * @param start the date to start from
     * @param windowLengthSeconds the length of the fasting window.
     * @return a HashMap containing nicely formatted dates as strings with their actual {@link ZonedDateTime}
     * as values that span over a duration defined by the start date and its windowLengthSeconds.
     */
    public static AllowedDays getDaysInRange(ZonedDateTime start, int windowLengthSeconds) {
        ZonedDateTime end = start.plusSeconds(windowLengthSeconds);

        // Don't just blindly convert the windowLengthSeconds to days. This would not account for scenarios
        // that overlap a day but the windowLengthSeconds is smaller than 24 hours.
        final long daysDuration = Duration.between(
                start.withHour(0).withMinute(0).withSecond(0).withNano(0),
                end.withHour(0).withMinute(0).withSecond(0).withNano(0)
        ).toDays();

        AllowedDays ret = new AllowedDays();

        int days = 0;
        while (days <= daysDuration) {
            ZonedDateTime tmp = start.plusDays(days);
            ret.addDay(formatDay(tmp), tmp);
            days += 1;
        }

        return ret;
    }

    /**
     * Formats days in a human readable format.
     *
     * @param pit
     * @return
     */
    public static String formatDay(ZonedDateTime pit) {
        if (isToday(pit)) {
            return "Today";
        }

        if (isYesterday(pit)) {
            return "Yesterday";
        }

        if (isTomorrow(pit)) {
            return "Tomorrow";
        }

        if (isWithinMonth(pit)) {
            return daysWithinCurrentMonth.format(pit);
        }

        return daysOutsideCurrentMonth.format(pit);
    }

    public static String formatDayTime(ZonedDateTime pit) {
        String time = formatter.format(pit);
        if (isYesterday(pit)) {
            return "Yesterday, " + time;
        }

        if (isTomorrow(pit)) {
            return "Tomorrow, " + time;
        }

        return time;
    }

    /**
     * Checks whether a given date was yesterdady.
     * @param time the point in time to check.
     *
     * @return true, if the given date was yesterday.
     */
    public static boolean isYesterday(ZonedDateTime time) {
        ZonedDateTime from = now().withHour(0).withMinute(0).withSecond(0).withNano(0).minusDays(1);
        ZonedDateTime to = from.plusDays(1);

        return time.isAfter(from) && time.isBefore(to);
    }

    public static boolean isToday(ZonedDateTime time) {
        ZonedDateTime from = now().withHour(0).withMinute(0).withSecond(0).withNano(0);
        ZonedDateTime to = from.plusDays(1);

        return time.isAfter(from) && time.isBefore(to);
    }

    public static boolean isTomorrow(ZonedDateTime time) {
        ZonedDateTime from = now().withHour(0).withMinute(0).withSecond(0).withNano(0);
        ZonedDateTime to = from.plusDays(1);

        return time.isAfter(from) && time.isBefore(to);
    }

    public static boolean isWithinMonth(ZonedDateTime time) {
        ZonedDateTime now = now();
        if (time.getMonthValue() == now.getMonthValue() && time.getYear() == now.getYear()) {
            return true;
        }

        return false;
    }

    /**
     * Resets the startdate of a record to now and also recalculates the target date.
     *
     * @param record the record to apply the operation to.
     */
    public static void reschedule(Record record) {
        record.setStartDate(now());
        record.setTargetDate(getTargetDate(record.getFastingWindowLength(), record.getStartDate()));
    }
}
