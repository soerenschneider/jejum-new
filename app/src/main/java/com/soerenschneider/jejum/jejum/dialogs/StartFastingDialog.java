package com.soerenschneider.jejum.jejum.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.*;

import com.soerenschneider.jejum.jejum.R;
import com.soerenschneider.jejum.jejum.utils.AllowedDays;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.IntStream;

public class StartFastingDialog extends AlertDialog implements DialogInterface.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private AllowedDays allowedDays;
    private Button start;
    private Button windowLength;

    /*
    private void recalc() {
        int hours = hoursPicker.getValue();
        int minutes = minutesPicker.getValue() * 5;
        this.fastingWindow = Integer.valueOf(hours * 3600 + minutes * 60);

        if (this.listener != null) {
        }
    }
    */

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == BUTTON_POSITIVE) {
        }
    }

    @Override
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> data, @Nullable Menu menu, int deviceId) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public StartFastingDialog(Context context) {
        this(context, null);
    }

    private void initUiElements(View view) {
        this.windowLength = view.findViewById(R.id.dialog_start_fasting_windowlength_button_hours);
        this.start = view.findViewById(R.id.dialog_start_fasting_start_button_daypicker);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        this.windowLength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        TextView tv = view.findViewById(R.id.dialog_start_fasting_windowlength_tv);
        tv.setText("Define custom window length");
    }

    public StartFastingDialog(Context context, CustomFastingTimeSet listener) {
        super(context);

        final Context themeContext = getContext();
        final LayoutInflater inflater = LayoutInflater.from(themeContext);
        final View view = inflater.inflate(R.layout.dialog_start_fasting_new, null);
        setView(view);

        initUiElements(view);

        setButton(BUTTON_POSITIVE, "OK", this);
        setButton(BUTTON_NEGATIVE, "Cancel", this);
    }

    public interface CustomFastingTimeSet {
        void customFastingTimeSet(int fastingWindowSeconds);
    }
}
