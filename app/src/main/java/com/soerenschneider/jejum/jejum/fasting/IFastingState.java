package com.soerenschneider.jejum.jejum.fasting;

import android.content.DialogInterface;
import android.view.View;

public interface IFastingState extends View.OnClickListener {
    State getFastingState();

    DialogInterface.OnClickListener getDialogButtonListener();

    void updateElements();
    void fromState(State state);

    /**
     * The states our fasting progress can have.
     */
    enum State {
        /** The user is currently fasting. */
        FASTING,

        /** The target time has been reached without any interruptions. Confirmation is still missing. */
        UNCONFIRMED,

        /** The fasting has been either been canceled or target reached and confirmed by the user. */
        RESTING;
    }
}
