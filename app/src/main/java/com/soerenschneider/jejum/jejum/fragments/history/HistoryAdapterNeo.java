package com.soerenschneider.jejum.jejum.fragments.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.soerenschneider.jejum.jejum.R;
import com.soerenschneider.jejum.jejum.data.Record;
import com.soerenschneider.jejum.jejum.utils.JejumDateUtils;

import java.util.List;

public class HistoryAdapterNeo extends RecyclerView.Adapter<HistoryAdapterNeo.MyViewHolder> {

    private List<Record> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tv_date;
        public TextView tv_target;
        public TextView tv_length;
        public TextView tv_diff;


        public MyViewHolder(View view) {
            super(view);
            tv_date = view.findViewById(R.id.list_date);
            tv_target = view.findViewById(R.id.list_target);
            tv_length = view.findViewById(R.id.list_duration);
            tv_diff = view.findViewById(R.id.list_diff);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public HistoryAdapterNeo(List<Record> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public HistoryAdapterNeo.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list_entry, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Record r = mDataset.get(position);

        int diff = r.getDiff();
        String prefix = "";
        if (diff > 0) {
            prefix = "+";
        }
        holder.tv_diff.setText(prefix+diff+"m");

        long start = r.getStart();
        long target = r.getTarget();
        int windowLength = r.getFastingWindowLength();

        // Populate fields with extracted properties
        holder.tv_date.setText(JejumDateUtils.formatDayTime(JejumDateUtils.fromUtc(start).get()));
        holder.tv_target.setText(JejumDateUtils.formatDayTime(JejumDateUtils.fromUtc(target).get()));
        holder.tv_length.setText(windowLength / 60 + "m");
    }

    public void setList(List<Record> records) {
        this.mDataset = records;
        notifyDataSetChanged();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
