package com.soerenschneider.jejum.jejum;

import android.os.CountDownTimer;
import android.util.Log;
import com.soerenschneider.jejum.jejum.utils.JejumDateUtils;

import java.time.ZonedDateTime;

public abstract class JejumCountdownTimer extends CountDownTimer {
    protected boolean isFinished = false;
    protected final ZonedDateTime targetDate;
    protected final int windowLength;

    protected static int interval = 1000;

    public JejumCountdownTimer(ZonedDateTime target, int windowLength) {
        super(1000 * (JejumDateUtils.secondsUntilTarget(target) + 1), interval);
        this.targetDate = target;
        this.windowLength = windowLength;
    }

    @Override
    public void onFinish() {
        isFinished = true;
        Log.i("jejum", "Timer finished");
    }

    public boolean isFinished() {
        return isFinished;
    }

    public ZonedDateTime getTargetDate() {
        return this.targetDate;
    }
}

