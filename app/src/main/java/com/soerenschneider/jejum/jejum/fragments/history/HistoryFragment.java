package com.soerenschneider.jejum.jejum.fragments.history;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.soerenschneider.jejum.jejum.R;
import com.soerenschneider.jejum.jejum.data.Record;
import com.soerenschneider.jejum.jejum.fasting.FastingViewModel;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HistoryFragment extends Fragment {

    private FastingViewModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View child = inflater.inflate(R.layout.fragment_history, container, false);
        RecyclerView lv = child.findViewById(R.id.fragment_history_listview);

        model = ViewModelProviders.of(getActivity()).get(FastingViewModel.class);
        HistoryAdapterNeo adapter = new HistoryAdapterNeo(new ArrayList<Record>());

        model.getRecords()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(records -> {
                    Log.e("jesus", ""+records);
                    adapter.setList(records);
                    adapter.notifyDataSetChanged();
                    } ,
                        throwable -> Log.e("bla", "Exception", throwable))
        ;

        lv.setAdapter(adapter);
        return child;
    }
}
