package com.soerenschneider.jejum.jejum.fasting.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import com.soerenschneider.jejum.jejum.R;

public class NotifierService extends BroadcastReceiver {
    private static final String CHANNEL_ID = "jejum";

    public static final long[] VIBRATION_PATTERN = {0, 400, 200, 400};

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        String text = "Happy lunch!";
        NotificationType type = NotificationType.valueOf(intent.getStringExtra("type"));
        if (NotificationType.IMMINENT == type) {
            int offset = intent.getIntExtra(context.getResources().getString(R.string.prefs_notifications_imminent_offset), -1);
            text = "You're good to go in " + offset + "mins";
        }

        NotificationCompat.Builder builder = notification(context).setStyle(new NotificationCompat.BigTextStyle().bigText(text));
        if (preferences.getBoolean(context.getResources().getString(R.string.prefs_notifications_finished_vibrate), false)) {
            builder.setVibrate(VIBRATION_PATTERN);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(0, builder.build());
    }

    public NotificationCompat.Builder  notification(Context context) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                //.setSmallIcon(R.drawable.baseline_alarm_black_18dp)
                .setContentTitle("Jejum")
                .setContentText("Fasting progress")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        return mBuilder;
    }
}