package com.soerenschneider.jejum.jejum.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.ContentValues;
import android.database.Cursor;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.soerenschneider.jejum.jejum.utils.JejumDateUtils;

import java.time.ZonedDateTime;
import java.util.Optional;

@Entity(tableName = "records")
public class Record {
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @ColumnInfo(name = "window_length")
    @NonNull
    private Integer fastingWindowLength;
    @ColumnInfo(name = "start")
    private Long start;
    @ColumnInfo(name = "target")
    private Long target;
    @ColumnInfo(name = "actual")
    private Long actual;
    @ColumnInfo(name = "diff")
    private Integer diff;

    public Record() {
    }

    public Record(int fastingWindowLength, ZonedDateTime start) {
        setFastingWindowLength(fastingWindowLength);
        setStartDate(start);
        setTargetDate(JejumDateUtils.getTargetDate(fastingWindowLength, start));
    }

    public Record(Cursor c) {
        id = Integer.valueOf(c.getInt(0));
        fastingWindowLength = Integer.valueOf(c.getInt(1));
        start = Long.valueOf(c.getLong(2));
        target = Long.valueOf(c.getLong(3));
        if (! c.isNull(4)) {
            actual = Long.valueOf(4);
        }

        if (! c.isNull(5)) {
            diff = Integer.valueOf(5);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        if (null == this.id) {
            this.id = id;
        }
    }

    public Long getStart() { return start;}

    public ZonedDateTime getStartDate() {
        return JejumDateUtils.fromUtc(start).get();
    }

    public void setStartDate(ZonedDateTime start) {
        this.start = JejumDateUtils.toUtc(start);
    }

    public Integer getFastingWindowLength() {
        return fastingWindowLength;
    }

    public void setFastingWindowLength(Integer fastingWindowLength) {
        if (fastingWindowLength < 8 * 3600 || fastingWindowLength > 36 * 3600) {
            throw new IllegalArgumentException();
        }

        this.fastingWindowLength = fastingWindowLength;
    }

    public Long getTarget() { return target; }

    public void setStart(Long start) {
        this.start = start;
    }

    public void setTarget(Long target) {
        this.target = target;
    }

    public void setActual(Long actual) {
        this.actual = actual;
    }

    public ZonedDateTime getTargetDate() {
        return JejumDateUtils.fromUtc(target).get();
    }

    public void setTargetDate(ZonedDateTime target) {
        this.target = JejumDateUtils.toUtc(target);
    }

    public Long getActual() { return actual; }

    public Optional<ZonedDateTime> getActualDate() {
        return JejumDateUtils.fromUtc(actual);
    }

    public void setActualDate(ZonedDateTime actual) {
        this.actual = JejumDateUtils.toUtc(actual);
        int diff = (int) JejumDateUtils.diff(JejumDateUtils.fromUtc(target).get(), actual).toMinutes();
        this.setDiff(diff);
    }

    public Integer getDiff() {
        return diff;
    }

    public void setDiff(Integer diff) {
        this.diff = diff;
    }

    public ContentValues toValues() {
        ContentValues values = new ContentValues();
        if (id != null) {
            values.put("id", id);
        }
        values.put("start", start);
        values.put("fastingWindowLength", fastingWindowLength);
        values.put("target", target);
        if (actual != null) {
            values.put("actual", actual);
        }
        if (diff != null) {
            values.put("diff", diff);
        }

        return values;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", fastingWindowLength=" + getFastingWindowLength() +
                ", start=" + getStartDate() +
                ", target=" + getTargetDate() +
                ", actual=" + getActualDate() +
                ", diff=" + getDiff() +
                '}';
    }

    public static DiffUtil.ItemCallback<Record> DIFF_CALLBACK = new DiffUtil.ItemCallback<Record>() {
        @Override
        public boolean areItemsTheSame(@NonNull Record oldItem, @NonNull Record newItem) {
            return oldItem.id == newItem.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Record oldItem, @NonNull Record newItem) {
            return oldItem.equals(newItem);
        }
    };
}
