package com.soerenschneider.jejum.jejum.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

import java.util.List;

@Dao
public interface JejumDao {
    @Query("SELECT * FROM records ORDER BY id DESC LIMIT 1")
    Flowable<Record> getMostRecent();

    @Query("SELECT * FROM records where id = :id")
    Maybe<Record> findById(int id);

    @Query("SELECT * FROM records ORDER BY id DESC")
    Flowable<List<Record>> getRecords();

    @Insert
    void insert(Record record);

    @Insert
    void insertAll(Record... record);

    @Delete
    void delete(Record record);

    @Update
    void updateRecords(Record... records);
}
